#include "mainwindow.h"
#include "mode_green.h"
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QMessageBox>
#include <QString>
#include <QtDebug>
#include <QToolBar>
#include <application.h>
#include <order_dialog.h>
#include "mode_yellow.h"
#include "mode_normal.h"

namespace PAYS {
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent){

    createActions();
    connect(m_ActionQuit, SIGNAL(triggered()), this, SLOT(close()));
    connect(m_ActionAbout, SIGNAL(triggered()), this, SLOT(on_about()));
    connect(m_ActionAboutQt, SIGNAL(triggered()), this, SLOT(on_about_qt()));
    connect(m_ActionNewOrder, SIGNAL(triggered()), this, SLOT(on_new_order()));
    connect(m_ActionYellowMode, &QAction::toggled, this, &MainWindow::on_yellow_mode);
    connect(m_ActionGreenMode, &QAction::toggled, this, &MainWindow::on_green_mode);
    connect(m_ActionNormalMode, &QAction::toggled, this, &MainWindow::on_normal_mode);

    QMenu *m_file = new QMenu(this);

    m_file->setTitle(tr("File"));
    menuBar()->addMenu(m_file);
    m_file->addSeparator();
    m_file->addAction(m_ActionQuit);
    m_file->addSeparator();
    m_file->addAction(m_ActionNewOrder);

    QMenu *test_menu = new QMenu(this);
    test_menu->setTitle(tr("Test"));
    menuBar()->addMenu(test_menu);

    QMenu *m_windows = new QMenu(this);

    m_windows->setTitle(tr("Windows"));
    menuBar()->addMenu(m_windows);
    m_windows->addAction(m_ActionGreenMode);
    m_windows->addAction(m_ActionYellowMode);
    m_windows->addAction(m_ActionNormalMode);

    QMenu *tools_menu = new QMenu(this);
    tools_menu->setTitle(tr("Tools"));
    menuBar()->addMenu(tools_menu);

    QMenu *m_help = new QMenu(this);

    m_help->setTitle(tr("Help"));
    menuBar()->addMenu(m_help);
    m_help->addAction(m_ActionHelp);
    m_help->addSeparator();
    m_help->addAction(m_ActionAbout);
    m_help->addAction(m_ActionAboutQt);

    m_ToolsOrder = new QToolBar(this);
    m_ToolsOrder->setWindowTitle(tr("Order"));
    addToolBar(Qt::TopToolBarArea, m_ToolsOrder);
    tools_menu->addAction(m_ToolsOrder->toggleViewAction());
    m_ToolsOrder->addAction(m_ActionNewOrder);


    QActionGroup *modes = new QActionGroup(this);
    modes->setExclusive(true);
    modes->addAction(m_ActionGreenMode);
    modes->addAction(m_ActionYellowMode);
    modes->addAction(m_ActionNormalMode);

}

MainWindow::~MainWindow(){}

void MainWindow::on_about_qt(){
    APP->aboutQt();
}

void MainWindow::on_about(){
    QString title = tr("About") + APP->applicationDisplayName();
    QString text = tr(
      "Starting in dir: C:/WORK/Qt/project3\n"
      "We have chosen to simplify your work"
    );
    QMessageBox::about(this, title, text);
}

void MainWindow::on_new_order(){
    QObject test;
    Order::Dialog Dia(this);
    Dia.attach(&test);
    Dia.exec();
    qDebug() << test.property("number");
    qDebug() << test.property("date");
    qDebug() << test.property("kind");
    qDebug() << test.property("payerStatus");
}

void MainWindow::on_yellow_mode(bool on){
    if(on){
        if(centralWidget()){
          qWarning()<<"Central Widjet is not empty";
        }
        ModeYellow *mode = new ModeYellow(this);
        setCentralWidget(mode);
    }else {
        if(!qobject_cast<ModeYellow*>(centralWidget())){
           qWarning()<<"Mode is not yellow";
        }
        centralWidget()->deleteLater();
        setCentralWidget(0);
    }
}

void MainWindow::on_green_mode(bool on){
    if(on){
        if(centralWidget()){
          qWarning()<<"Central Widjet is not empty";
        }
        ModeGreen *mode = new ModeGreen(this);
        setCentralWidget(mode);
    }else {
        if(!qobject_cast<ModeGreen*>(centralWidget())){
           qWarning()<<"Mode is not green";
        }
        centralWidget()->deleteLater();
        setCentralWidget(0);
    }
}


void MainWindow::on_normal_mode(bool on){
    if(on){
        if(centralWidget()){
          qWarning()<<"Central Widjet is not empty";
        }
        ModeNormal *mode = new ModeNormal(this);
        setCentralWidget(mode);
    }else {
        if(!qobject_cast<ModeNormal*>(centralWidget())){
           qWarning()<<"Mode is not normal";
        }
        centralWidget()->deleteLater();
        setCentralWidget(0);
    }
}
} // namespace PAYS

