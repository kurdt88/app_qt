#ifndef APPLICATION_H
#define APPLICATION_H
#include <QApplication>
#include <QTranslator>

namespace PAYS {
class Application : public QApplication{
    Q_OBJECT
    private:
    QTranslator *m_Translator;
    public:
      Application (int argc, char* argvp[]);
      virtual ~Application(){}
};
} // namespace PAYS
#define APP qobject_cast<::PAYS::Application*>(qApp)
#endif // APPLICATION_H

