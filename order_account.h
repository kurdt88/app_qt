#ifndef ORDER_ACCOUNT_H
#define ORDER_ACCOUNT_H

#include <QFrame>
#include <ui_account.h>

namespace PAYS {
namespace Order {
class Account : public QFrame, private Ui::Account{
    Q_OBJECT
private:
    Ui::Account ui;
public:
    Account(QWidget *parent=0);
    virtual ~Account() {}
    void setName(const QString &name);
};
} // namespace Order
} // namespace PAYS
#endif // ORDER_ACCOUNT_H
