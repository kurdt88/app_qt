#include "mainwindow.h"
#include "application.h"

int main(int argc, char *argv[])
{
    PAYS::Application app(argc, argv);
    PAYS::MainWindow window;
    window.showMaximized();
    return app.exec();
}
