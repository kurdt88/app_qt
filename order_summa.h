#ifndef ORDER_SUMMA_H
#define ORDER_SUMMA_H

#include <QFrame>
#include <ui_summ.h>

namespace PAYS {
namespace Order {
class Summa : public QFrame
{
    Q_OBJECT
    Q_PROPERTY(QString value READ value WRITE setValue NOTIFY value_changed)
private:
    Ui::Summa ui;
public:
    Summa(QWidget *parent=0);
    virtual ~Summa() {}
    QString value() const;
    void setValue(const QString &value);

    signals:
    void value_changed(const QString &);
public slots:
    void can_accept(bool *OK);
};
} // namespace Order
} // namespace PAYS
#endif // ORDER_SUMMA_H
