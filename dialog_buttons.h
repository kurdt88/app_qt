#ifndef DIALOG_BUTTONS_H
#define DIALOG_BUTTONS_H
#include <QFrame>

class QPushButton;
class QLabel;

namespace PAYS {
namespace Dialogs {
class Buttons : public QFrame
{
    Q_OBJECT

private:
    QLabel *m_ErrorMsg;
    QPushButton *m_OK;
    QPushButton *m_Cancel;
public:
    Buttons(QWidget *parent=0);
    virtual ~Buttons() {}
public slots:
    void error_msg(const QString &msg);
private slots:
    void on_ok_pressed();
signals:
    void rejected();
    void accepted();
    void accept_request(bool *OK);
    void save_request(bool *OK);
};
} // namespace Dialogs
} // namespace PAYS
#endif // DIALOG_BUTTONS_H
