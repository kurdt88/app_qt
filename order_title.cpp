#include "order_title.h"
#include <QLabel>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QDateEdit>
#include <QComboBox>
#include <QtDebug>

namespace PAYS {
namespace Order {

Title::Title(QWidget *parent)
    :QFrame(parent), m_Data(nullptr){

    QHBoxLayout * layout = new QHBoxLayout(this);
    layout->setMargin(0);

    layout->addStretch();

    QLabel *order_title = new QLabel(this);
    order_title->setText(tr("PAYMENT ORDER"));
    layout->addWidget(order_title);

    m_Number = new QLineEdit(this);
    m_Number->setMaxLength(6);
    layout->addWidget(m_Number);

    m_Date = new QDateEdit(this);
    m_Date->setDisplayFormat("dd.MM.yyyy");
    m_Date->setCalendarPopup(true);
    m_Date->setDateTime(QDateTime::currentDateTime());

    layout->addWidget(m_Date);

    m_Kind = new QComboBox(this);

    m_Kind->addItems(setKindList());
    layout->addWidget(m_Kind);
    layout->addStretch();

    m_PayerStatus = new QLineEdit(this);
    m_PayerStatus->setMaxLength(2);
    m_PayerStatus->setInputMask("99:_");
    m_PayerStatus->setMaximumWidth(100);
    layout->addWidget(m_PayerStatus);

}

QString Title::number() const{
    return m_Number->text().simplified();
}

void Title::setNumber(const QString &value){
    m_Number->setText(value.simplified());
}

QDateTime Title::date() const{
    return m_Date->dateTime();
}

void Title::setDate(const QDateTime &value){
   m_Date->setDateTime(value);
}

QString Title::kind() const{
    return m_Kind->currentText();
}
void Title::setKind(const QString &value){
    QString val = value.simplified();
    if (val==QString::fromUtf8("электронно")){
       m_Kind->setCurrentIndex(0);
    }else if (val==QString::fromUtf8("почтой")){
       m_Kind->setCurrentIndex(1);
    }else if (val==QString::fromUtf8("телеграфом")){
       m_Kind->setCurrentIndex(2);
    }else if (val==QString::fromUtf8("срочно")){
       m_Kind->setCurrentIndex(3);
    }else {
        qWarning() << "Invalid payment kind " << val;
    }
}

QString Title::payerStatus() const{
    return m_PayerStatus->text();
}

void Title::setPayerStatus(const QString &value){
    m_PayerStatus->setText(value.simplified());
}

QStringList Title::setKindList() {
    m_KindList = new QStringList();
    m_KindList->insert(0, QString::fromUtf8("электронно"));
    m_KindList->insert(1, QString::fromUtf8("почтой"));
    m_KindList->insert(2, QString::fromUtf8("телеграфом"));
    m_KindList->insert(3, QString::fromUtf8("срочно"));
    return *m_KindList;
}

void Title::can_accept(bool *OK){
    if(!m_PayerStatus->hasAcceptableInput()){
        emit raise_error(tr("Invalid Payer Status"));
        m_PayerStatus->setStyleSheet("background : #FFCCCC");
        *OK = false;
        return;
    }
}

void Title::save_data(bool* /*OK*/){
    if(!m_Data) return;
    const char *props[] = {"number", "date", "kind", "payerStatus", 0};
    for (int k = 0; props[k]; ++k) {
        m_Data->setProperty(props[k], property(props[k]));
    }
}
} // namespace Order
} // namespace PAYS
