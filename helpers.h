#ifndef HELPERS_H
#define HELPERS_H
#include <QString>
namespace PAYS {
namespace HLP {
QString sum_as_words(QString value);
} // namespace HLP
} // namespace PAYS
#endif // HELPERS_H
