#include "application.h"

namespace PAYS {
Application::Application(int argc, char* argv[])
    :QApplication(argc, argv){
    setApplicationName("Pays");
    setApplicationDisplayName(tr("Pays"));
    setApplicationVersion(tr("0.1"));

    m_Translator = new QTranslator(this);
    m_Translator->load(":/l10n/ru_Ru.qm");
    installTranslator(m_Translator);

}
} // namespace PAYS
