#ifndef ORDER_BANK_NAME_H
#define ORDER_BANK_NAME_H

#include <QFrame>
#include <ui_bank_name.h>

namespace PAYS {
namespace Order {
class BankName : public QFrame
{
    Q_OBJECT
private:
    Ui::BankName ui;
public:
    BankName(QWidget *Parent = 0);
    virtual ~BankName(){}
    void setName(const QString &name);
};
} // namespace Order
} // namespace PAYS


#endif // ORDER_BANK_NAME_H
