#ifndef MODE_YELLOW_H
#define MODE_YELLOW_H

#include <QFrame>

namespace PAYS {
class ModeYellow : public QFrame
{
    Q_OBJECT
public:
    ModeYellow(QWidget *parent= 0);
    virtual ~ModeYellow(){}
};
} // namespace PAYS
#endif // MODE_GREEN_H
