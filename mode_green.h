#ifndef MODE_GREEN_H
#define MODE_GREEN_H

#include <QFrame>

namespace PAYS {
class ModeGreen : public QFrame
{
    Q_OBJECT
public:
    ModeGreen(QWidget *parent= 0);
    virtual ~ModeGreen(){}
};
} // namespace PAYS
#endif // MODE_GREEN_H
