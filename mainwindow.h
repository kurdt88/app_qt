#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QAction>
#include <QToolBar>

namespace PAYS {
class MainWindow : public QMainWindow
{
    Q_OBJECT
private:

    QAction *m_ActionQuit;
    QAction *m_ActionHelp;
    QAction *m_ActionAbout;
    QAction *m_ActionAboutQt;
    QAction *m_ActionNewOrder;
    QToolBar *m_ToolsOrder;
    QAction *m_ActionYellowMode;
    QAction *m_ActionGreenMode;
    QAction *m_ActionNormalMode;

    void createActions(){

        m_ActionQuit = new QAction(this);
        m_ActionQuit->setText(tr("Quit"));

        m_ActionNewOrder = new QAction(this);
        m_ActionNewOrder->setText(tr("New Order"));
        m_ActionNewOrder->setShortcut(Qt::ALT + Qt::Key_N);
        m_ActionNewOrder->setIcon(QIcon(":/icons/new.svg"));

        m_ActionQuit->setShortcut(Qt::ALT + Qt::Key_F4);
        m_ActionQuit->setIcon(QIcon(":/icons/exit.svg"));

        m_ActionHelp = new QAction(this);
        m_ActionHelp->setText(tr("Help"));

        m_ActionAbout = new QAction(this);
        m_ActionAbout->setText(tr("About ..."));

        m_ActionAboutQt = new QAction(this);
        m_ActionAboutQt->setText(tr("AboutQt ...")); 

        m_ActionYellowMode = new QAction(this);
        m_ActionYellowMode->setText(tr("Yellow"));
        m_ActionYellowMode->setCheckable(true);
        m_ActionYellowMode->setChecked(false);

        m_ActionGreenMode = new QAction(this);
        m_ActionGreenMode->setText(tr("Green"));
        m_ActionGreenMode->setCheckable(true);
        m_ActionGreenMode->setChecked(false);

        m_ActionNormalMode = new QAction(this);
        m_ActionNormalMode->setText(tr("Normal"));
        m_ActionNormalMode->setCheckable(true);
        m_ActionNormalMode->setChecked(true);
    }

public slots:

    void on_about_qt();
    void on_about();
    void on_new_order();

public:

    MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();

private slots:
    void on_yellow_mode(bool on);
    void on_green_mode(bool on);
    void on_normal_mode(bool on);

};
} // namespace PAYS
#endif // MAINWINDOW_H
