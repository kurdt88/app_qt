#include "order_summa.h"
#include <QRegExpValidator>

namespace PAYS {
namespace Order {
Summa::Summa(QWidget *parent)
    :QFrame(parent){

    ui.setupUi(this);
    QRegExp rx("^\\d*[\\.,]?\\d?\\d?$");
    QValidator *validator = new QRegExpValidator(rx, this);
    ui.m_Value->setValidator(validator);

    connect(ui.m_Value, SIGNAL(textEdited(const QString&)), this, SIGNAL(value_changed(const QString&)));
}

QString Summa::value() const {
    return ui.m_Value->text().simplified();
}

void Summa::setValue(const QString &value){
    ui.m_Value->setText(value.simplified());
}
//FixMe
//Сделать подсветку при "плохом" вводе и выводить сообщение об ошибке
void Summa::can_accept(bool *OK){
    if (! ui.m_Value->hasAcceptableInput()){
        *OK =false;
    }
}
} // namespace Order

} // namespace PAYS
