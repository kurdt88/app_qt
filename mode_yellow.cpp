#include "mode_yellow.h"

namespace PAYS {
ModeYellow::ModeYellow(QWidget *parent)
    :QFrame(parent)
{
    setStyleSheet("background: #BDB76B");
}
} // namespace PAYS

