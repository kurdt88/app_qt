#include "order_dialog.h"
#include "order_frame.h"

namespace PAYS {
namespace Order {
Dialog::Dialog(QWidget *parent)
    :Dialogs::Base (parent){
    Frame * f = new Frame(this);
    setCentralFrame(f);
}
} // namespace Order
} // namespace PAYS
