#ifndef DIALOG_COMMON_H
#define DIALOG_COMMON_H

#include <QDialog>

class QFrame;

namespace PAYS {
namespace Dialogs {
class Base : public QDialog
{
    Q_OBJECT

private:

    QFrame *m_Central;
    QFrame *m_Buttons;
public:
    Base(QWidget *parent = 0);
    virtual ~Base() {}
    void setCentralFrame(QFrame *central);
    virtual void attach(QObject *data);

signals:
    void attach_data(QObject *data);
};
} // namespace Dialogs
} // namespace PAYS
#endif // DIALOG_COMMON_H
