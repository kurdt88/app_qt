#include "mode_green.h"

namespace PAYS {
ModeGreen::ModeGreen(QWidget *parent)
    :QFrame(parent)
{
    setStyleSheet("background: #CCFFCC");
}
} // namespace PAYS

