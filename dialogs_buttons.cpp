#include "dialog_buttons.h"
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>

namespace PAYS {
namespace Dialogs {
Buttons::Buttons(QWidget * parent)
    :QFrame(parent){

    QHBoxLayout *layout = new QHBoxLayout(this);

    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);

    m_ErrorMsg = new QLabel(this);
    //m_ErrorMsg->setText("Test Error MSG");
    layout->addWidget(m_ErrorMsg);
    m_ErrorMsg->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    m_ErrorMsg->setStyleSheet("color: red");

    m_OK = new QPushButton(this);
    m_OK->setText("OK");
    layout->addWidget(m_OK);

    m_Cancel = new QPushButton(this);
    m_Cancel->setText("Cancel");
    layout->addWidget(m_Cancel);

    connect(m_Cancel, SIGNAL(clicked()), this, SIGNAL(rejected()));
    connect(m_OK, SIGNAL(clicked()), this, SLOT(on_ok_pressed()));
}
void Buttons::on_ok_pressed(){
    bool OK = true;
    emit accept_request(&OK);
    if (!OK) return;
    emit save_request(&OK);
    if (!OK) return;
    emit accepted();
}
void Buttons::error_msg(const QString &msg){
    m_ErrorMsg->setText(msg);
}
} // namespace Dialogs
} // namespace PAYS
