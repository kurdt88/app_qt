#ifndef MODE_NORMAL_H
#define MODE_NORMAL_H

#include <QFrame>

namespace PAYS {
class ModeNormal : public QFrame
{
    Q_OBJECT
public:
    ModeNormal(QWidget *parent = 0);
    virtual ~ModeNormal(){}

};
} // namespace PAYS
#endif // MODE_NORMAL_H
