#include "order_account.h"

namespace PAYS {
namespace Order {
Account::Account(QWidget *parent)
    :QFrame (parent){
    setupUi(this);
}
void Account:: setName(const QString &name){
    m_Label->setText(name);
}
} // namespace Order
} // namespace PAYS
